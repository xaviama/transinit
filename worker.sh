#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"

sh index.sh

apk add --no-cache sed

sed -i 's/index.html//g' .gitignore
